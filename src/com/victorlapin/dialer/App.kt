package com.victorlapin.dialer

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.color.DynamicColors
import com.google.android.material.color.DynamicColorsOptions
import com.victorlapin.dialer.provider.SettingsProvider
import org.koin.android.ext.android.inject

class App : Application() {
    private val settings by inject<SettingsProvider>()

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(settings.nightMode)
        DynamicColors.applyToActivitiesIfAvailable(
            this,
            DynamicColorsOptions.Builder().setThemeOverlay(R.style.AppTheme_DynamicOverlay).build()
        )
    }
}