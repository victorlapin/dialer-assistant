package com.victorlapin.dialer

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.view.ViewTreeObserver
import androidx.annotation.DimenRes

/**
 * Created by victor on 05.03.2020.
 */
fun <T : View> T.height(function: (Int) -> Unit) {
    if (height == 0)
        viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                viewTreeObserver.removeOnPreDrawListener(this)
                function(height)
                return true
            }
        })
    else function(height)
}

fun openSystemSettings(context: Context) {
    val intent = Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.parse("package:${context.packageName}")
    ).apply {
        addCategory(Intent.CATEGORY_DEFAULT)
    }
    context.startActivity(intent)
}

fun View.getPixels(@DimenRes id: Int) = context.resources.getDimensionPixelSize(id)