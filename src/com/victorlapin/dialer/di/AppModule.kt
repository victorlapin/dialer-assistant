package com.victorlapin.dialer.di

import com.victorlapin.dialer.provider.ServicesProvider
import com.victorlapin.dialer.provider.SettingsProvider
import org.koin.dsl.module

val appModule = module {
    single { SettingsProvider(get()) }
    single { ServicesProvider(get()) }
}