package com.victorlapin.dialer.model

data class ContactData(
    val id: Int,
    var firstName: String? = null,
    var middleName: String? = null,
    var lastName: String? = null,
    var jobCompany: String? = null,
    var jobTitle: String? = null,
    var note: String? = null
)