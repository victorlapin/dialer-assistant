package com.victorlapin.dialer.provider

import android.Manifest
import android.Manifest.permission.SYSTEM_ALERT_WINDOW
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.role.RoleManager
import android.content.Context
import android.content.pm.PackageManager
import android.view.WindowManager
import com.victorlapin.dialer.ui.services.AssistantService

/**
 * Created by victor on 25.02.2020.
 */
class ServicesProvider(private val context: Context) {

    private val appOpsManager by lazy {
        context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
    }

    val windowManager by lazy {
        context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    }

    private val roleManager by lazy {
        context.getSystemService(Context.ROLE_SERVICE) as RoleManager
    }

    private val activityManager by lazy {
        context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    }

    fun isOverlayGranted(): Boolean {
        val granted: Boolean
        val mode = appOpsManager.unsafeCheckOpNoThrow(
            AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
            android.os.Process.myUid(), context.packageName
        )

        granted = if (mode == AppOpsManager.MODE_DEFAULT) {
            context.checkCallingOrSelfPermission(SYSTEM_ALERT_WINDOW) ==
                    PackageManager.PERMISSION_GRANTED
        } else {
            mode == AppOpsManager.MODE_ALLOWED
        }

        return granted
    }

    fun canReadContacts(): Boolean =
        context.checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED

    fun canReadPhoneState(): Boolean =
        context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED

    fun checkScreeningRole(): Boolean =
        roleManager.isRoleHeld(RoleManager.ROLE_CALL_SCREENING)

    val screeningIntent by lazy {
        roleManager.createRequestRoleIntent(RoleManager.ROLE_CALL_SCREENING)
    }

    fun isAssistantRunning(): Boolean = activityManager.getRunningServices(3)
        .any { it.service.className == AssistantService::class.qualifiedName }
}