package com.victorlapin.dialer.provider

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import com.victorlapin.dialer.R

/**
 * Created by victor on 25.02.2020.
 */
class SettingsProvider(context: Context) {
    companion object {
        const val KEY_NIGHT_MODE = "interface_night_mode"
        const val KEY_CONTACTS_PERMISSION = "security_contacts_permission"
        const val KEY_PHONE_PERMISSION = "security_phone_permission"
        const val KEY_OVERLAY_PERMISSION = "security_overlay_permission"
        const val KEY_DISPLAY_FIELDS = "interface_display_fields"
        const val KEY_PLACEMENT = "interface_placement"
        const val KEY_SCREENING_APP = "security_screening_app"
        const val KEY_APP_VERSION = "info_app_version"
    }

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)
    private val defaultFields =
        context.resources.getStringArray(R.array.display_fields_ids).joinToString(separator = ",")

    var nightMode: Int
        get() = prefs.getInt(KEY_NIGHT_MODE, AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        set(mode) = prefs.edit().putInt(KEY_NIGHT_MODE, mode).apply()

    var displayFields: List<String>
        get() {
            val flattenedString = prefs.getString(KEY_DISPLAY_FIELDS, defaultFields)!!
            return flattenedString.split(",")
        }
        set(value) {
            val flattenedString = value.joinToString(separator = ",")
            prefs.edit().putString(KEY_DISPLAY_FIELDS, flattenedString).apply()
        }

    var assistantViewInitialYPercent: Int
        get() = prefs.getInt(KEY_PLACEMENT, 40)
        set(value) = prefs.edit().putInt(KEY_PLACEMENT, value).apply()
}