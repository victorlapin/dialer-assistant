package com.victorlapin.dialer.startup

import android.content.Context
import androidx.startup.Initializer
import com.victorlapin.dialer.BuildConfig
import com.victorlapin.dialer.di.allModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Created by victor on 23.09.2021.
 */
class KoinInitializer : Initializer<Any> {

    override fun create(context: Context): Any {
        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.DEBUG else Level.NONE)
            androidContext(context)
            modules(allModules)
        }

        return Any()
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> = mutableListOf()
}