package com.victorlapin.dialer.startup

import android.content.Context
import androidx.startup.Initializer
import com.victorlapin.dialer.BuildConfig
import timber.log.Timber

/**
 * Created by victor on 23.09.2021.
 */
class LogInitializer : Initializer<Any> {

    override fun create(context: Context): Any {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        return Any()
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> = mutableListOf()
}