package com.victorlapin.dialer.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.util.forEach
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.victorlapin.dialer.BuildConfig
import com.victorlapin.dialer.R
import com.victorlapin.dialer.getPixels
import com.victorlapin.dialer.openSystemSettings
import com.victorlapin.dialer.provider.ServicesProvider
import com.victorlapin.dialer.provider.SettingsProvider
import org.koin.android.ext.android.inject

/**
 * Created by victor on 25.02.2020.
 */
class SettingsGlobalFragment : PreferenceFragmentCompat(), DefaultLifecycleObserver {
    private val settings by inject<SettingsProvider>()
    private val services by inject<ServicesProvider>()

    private val permissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) onResume(this.viewLifecycleOwner)
        }

    private val screeningRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            val screeningPref = findPreference<Preference>(SettingsProvider.KEY_SCREENING_APP)
            if (it.resultCode == Activity.RESULT_OK) {
                screeningPref?.summary = getString(R.string.pref_screening_enabled)
            } else {
                screeningPref?.summary = getString(R.string.pref_screening_disabled)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super<PreferenceFragmentCompat>.onCreate(savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView.setPadding(0,0,0, view.getPixels(R.dimen.margin_64))
        listView.clipToPadding = false
    }

    @SuppressLint("BatteryLife")
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_global)

        val nightPref = findPreference<Preference>(SettingsProvider.KEY_NIGHT_MODE)
        val entries = arrayOf(
            getString(R.string.pref_night_mode_system),
            getString(R.string.pref_night_mode_on),
            getString(R.string.pref_night_mode_off)
        )
        val values = arrayOf(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM,
            AppCompatDelegate.MODE_NIGHT_YES,
            AppCompatDelegate.MODE_NIGHT_NO
        )
        val initialSelection = values.indexOf(settings.nightMode)
        nightPref?.summary = entries[initialSelection]
        nightPref?.setOnPreferenceClickListener {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.pref_title_night_mode)
                .setSingleChoiceItems(entries, initialSelection, null)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    val index = (dialog as AlertDialog).listView.checkedItemPosition
                    val theme = values[index]
                    settings.nightMode = theme
                    AppCompatDelegate.setDefaultNightMode(theme)
                    it.summary = entries[index]
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
            true
        }

        val contactsPref = findPreference<Preference>(SettingsProvider.KEY_CONTACTS_PERMISSION)
        contactsPref?.setOnPreferenceClickListener {
            if (!services.canReadContacts()) {
                permissionRequest.launch(Manifest.permission.READ_CONTACTS)
            } else {
                openSystemSettings(requireContext())
            }
            true
        }

        val phonePref = findPreference<Preference>(SettingsProvider.KEY_PHONE_PERMISSION)
        phonePref?.setOnPreferenceClickListener {
            if (!services.canReadPhoneState()) {
                permissionRequest.launch(Manifest.permission.READ_PHONE_STATE)
            } else {
                openSystemSettings(requireContext())
            }
            true
        }

        val screeningPref = findPreference<Preference>(SettingsProvider.KEY_SCREENING_APP)
        screeningPref?.setOnPreferenceClickListener {
            screeningRequest.launch(services.screeningIntent)
            true
        }

        val overlayPref = findPreference<Preference>(SettingsProvider.KEY_OVERLAY_PERMISSION)
        overlayPref?.setOnPreferenceClickListener {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION).apply {
                data = Uri.parse("package:${requireContext().packageName}")
            }
            startActivity(intent)
            true
        }

        val fieldsPref = findPreference<Preference>(SettingsProvider.KEY_DISPLAY_FIELDS)
        val fieldsValues = requireContext().resources.getStringArray(R.array.display_fields_values)
        val fieldsIds = requireContext().resources.getStringArray(R.array.display_fields_ids)
        val fieldsSummary = mutableListOf<String>()
        settings.displayFields.forEach {
            val index = fieldsIds.indexOf(it)
            fieldsSummary.add(fieldsValues[index])
        }
        fieldsPref?.summary = fieldsSummary.joinToString()
        fieldsPref?.setOnPreferenceClickListener {
            val selected = settings.displayFields
            val selectedIndices = BooleanArray(fieldsIds.size) {
                selected.contains(fieldsIds[it])
            }

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.pref_title_display_fields)
                .setMultiChoiceItems(fieldsValues, selectedIndices, null)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    val newSummary = mutableListOf<String>()
                    val newIds = mutableListOf<String>()
                    val indices = (dialog as AlertDialog).listView.checkedItemPositions
                    indices.forEach { k, v ->
                        if (v) {
                            newSummary.add(fieldsValues[k])
                            newIds.add(fieldsIds[k])
                        }
                    }
                    it.summary = newSummary.joinToString()
                    settings.displayFields = newIds
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()

            true
        }

        val versionPref = findPreference<Preference>(SettingsProvider.KEY_APP_VERSION)
        versionPref?.summary = BuildConfig.VERSION_NAME
    }

    override fun onResume(owner: LifecycleOwner) {
        super<DefaultLifecycleObserver>.onResume(owner)

        if (owner.lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            val contactsPref = findPreference<Preference>(SettingsProvider.KEY_CONTACTS_PERMISSION)
            contactsPref?.summary =
                if (services.canReadContacts()) getString(R.string.pref_contacts_granted)
                else getString(R.string.pref_contacts_denied)

            val phonePref = findPreference<Preference>(SettingsProvider.KEY_PHONE_PERMISSION)
            phonePref?.summary =
                if (services.canReadPhoneState()) getString(R.string.pref_phone_granted)
                else getString(R.string.pref_phone_denied)

            val overlayPref = findPreference<Preference>(SettingsProvider.KEY_OVERLAY_PERMISSION)
            overlayPref?.summary =
                if (services.isOverlayGranted()) getString(R.string.pref_overlays_granted)
                else getString(R.string.pref_overlays_denied)

            val screeningPref = findPreference<Preference>(SettingsProvider.KEY_SCREENING_APP)
            screeningPref?.summary =
                if (services.checkScreeningRole()) getString(R.string.pref_screening_enabled)
                else getString(R.string.pref_screening_disabled)
        }
    }
}