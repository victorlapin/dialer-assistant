package com.victorlapin.dialer.ui.preferences

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.SeekBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.victorlapin.dialer.R
import com.victorlapin.dialer.databinding.PreferencePlacementBinding
import com.victorlapin.dialer.height

/**
 * Created by victor on 04.03.2020.
 */
class ViewPlacementPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : Preference(context, attrs, defStyleAttr) {
    init {
        layoutResource = R.layout.preference_placement
    }

    private var marginMultiplier: Int = -1

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        with(PreferencePlacementBinding.bind(holder.itemView)) {
            this.root.background = null
            phoneBackground.height {
                marginMultiplier = it / 100
                movePreview(assistantPreview.root, getPersistedInt(40))
            }
            seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    if (fromUser) {
                        persistInt(progress)
                    }

                    if (marginMultiplier > -1) {
                        movePreview(assistantPreview.root, progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}

                override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            })
            seekbar.progress = getPersistedInt(40)
        }
    }

    private fun movePreview(preview: View, progress: Int) {
        val params = preview.layoutParams as ConstraintLayout.LayoutParams
        params.topMargin = marginMultiplier * progress
        preview.layoutParams = params
    }
}