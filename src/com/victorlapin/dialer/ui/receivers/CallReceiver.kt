package com.victorlapin.dialer.ui.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import com.victorlapin.dialer.provider.ServicesProvider
import com.victorlapin.dialer.ui.services.AssistantService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by victor on 25.02.2020.
 */
class CallReceiver : BroadcastReceiver(), KoinComponent {
    private val services by inject<ServicesProvider>()

    override fun onReceive(context: Context?, intent: Intent?) {
        if (TelephonyManager.ACTION_PHONE_STATE_CHANGED == intent?.action) {
            if (intent.extras != null) {
                val it = intent.extras!!

                when (it.getString(TelephonyManager.EXTRA_STATE)) {
                    TelephonyManager.EXTRA_STATE_OFFHOOK,
                    TelephonyManager.EXTRA_STATE_IDLE -> {
                        if (services.isAssistantRunning()) {
                            val assistantIntent = Intent(context!!, AssistantService::class.java)
                            context.stopService(assistantIntent)
                        }
                    }
                }
            }
        }
    }
}