package com.victorlapin.dialer.ui.services

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.content.res.Configuration
import android.graphics.PixelFormat
import android.net.Uri
import android.os.IBinder
import android.provider.ContactsContract
import android.view.*
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import com.victorlapin.dialer.R
import com.victorlapin.dialer.databinding.CallAssistantBinding
import com.victorlapin.dialer.model.ContactData
import com.victorlapin.dialer.provider.ServicesProvider
import com.victorlapin.dialer.provider.SettingsProvider
import org.koin.android.scope.AndroidScopeComponent
import org.koin.android.scope.serviceScope
import org.koin.core.scope.Scope


/**
 * Created by victor on 25.02.2020.
 */
class AssistantService : Service(), View.OnTouchListener, AndroidScopeComponent {
    override val scope: Scope by serviceScope()
    private val settings by scope.inject<SettingsProvider>()
    private val services by scope.inject<ServicesProvider>()

    private var assistantView: View? = null

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val phone = intent.extras?.getString(EXTRA_PHONE)
            if (phone != null) {
                showAssist(phone)
            } else {
                stopSelf(startId)
            }
        } else {
            stopSelf(startId)
        }
        return START_NOT_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        cleanup()
        super.onTaskRemoved(rootIntent)
    }

    override fun onDestroy() {
        cleanup()
        super.onDestroy()
    }

    private fun cleanup() {
        removeView()
        scope.close()
    }

    private fun showAssist(phoneNumber: String) {
        if (services.canReadContacts()) {
            val data = searchContact(phoneNumber)
            data?.let {
                if (services.isOverlayGranted()) {
                    createView(it)
                    showView()
                }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    private fun createView(data: ContactData) {
        val fields = settings.displayFields
        val sb = StringBuilder()

        val nightMode = when (settings.nightMode) {
            AppCompatDelegate.MODE_NIGHT_YES -> Configuration.UI_MODE_NIGHT_YES
            AppCompatDelegate.MODE_NIGHT_NO -> Configuration.UI_MODE_NIGHT_NO
            else -> resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        }
        val configuration = Configuration().apply {
            uiMode = nightMode
        }
        val themedContext = ContextThemeWrapper(this, R.style.AppTheme).apply {
            applyOverrideConfiguration(configuration)
        }
        assistantView = LayoutInflater.from(themedContext)
            .inflate(R.layout.call_assistant, null)
        with(CallAssistantBinding.bind(assistantView!!)) {
            root.setOnTouchListener(this@AssistantService)
            if (fields.contains(getString(R.string.field_id_first_name)) && data.firstName != null) {
                sb.append(data.firstName)
                sb.append(" ")
            }
            if (fields.contains(getString(R.string.field_id_middle_name)) && data.middleName != null) {
                sb.append(data.middleName)
                sb.append(" ")
            }
            if (fields.contains(getString(R.string.field_id_last_name)) && data.lastName != null) {
                sb.append(data.lastName)
            }
            if (sb.isNotEmpty()) {
                lblName.text = sb.toString()
                sb.clear()
            } else {
                lblName.isVisible = false
            }

            if (fields.contains(getString(R.string.field_id_job_company)) && data.jobCompany != null) {
                sb.append(data.jobCompany)
            }
            if (fields.contains(getString(R.string.field_id_job_title)) && data.jobTitle != null) {
                if (sb.isNotEmpty()) {
                    sb.append(", ")
                }
                sb.append(data.jobTitle)
            }
            if (sb.isNotEmpty()) {
                lblJob.text = sb.toString()
                sb.clear()
            } else {
                lblJob.isVisible = false
            }

            if (fields.contains(getString(R.string.field_id_note)) && data.note != null) {
                lblNote.text = data.note
            }
            if (lblNote.text.isNullOrEmpty()) {
                lblNote.isVisible = false
            }
        }
    }

    private fun showView() {
        val screenSize = services.windowManager.currentWindowMetrics.bounds

        val params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            0,
            screenSize.height() / 100 * settings.assistantViewInitialYPercent,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                    or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
            PixelFormat.TRANSPARENT
        ).apply {
            gravity = Gravity.TOP or Gravity.START
        }
        services.windowManager.addView(assistantView, params)
    }

    private fun removeView() {
        assistantView?.let {
            services.windowManager.removeView(it)
        }
        assistantView = null
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_MOVE) {
            val newY = (event.rawY - v.height / 2).toInt()

            val params = (assistantView?.layoutParams as WindowManager.LayoutParams).apply {
                y = newY
            }
            services.windowManager.updateViewLayout(assistantView, params)
            return true
        }
        return false
    }

    private fun searchContact(phoneNumber: String): ContactData? {
        var result: ContactData? = null

        // lookup contact by phone number first
        val uri = Uri.withAppendedPath(
            ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
            Uri.encode(phoneNumber)
        )
        contentResolver.query(
            uri,
            arrayOf(ContactsContract.PhoneLookup.CONTACT_ID),
            null,
            null,
            null
        ).use {
            if (it != null && it.moveToFirst()) {
                result = ContactData(id = it.getInt(0))
            }
        }

        // now retrieve data
        result?.let {
            contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(
                    ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                    ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
                    ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME
                ),
                ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                arrayOf(
                    it.id.toString(),
                    ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
                ),
                null
            ).use { c ->
                if (c != null && c.moveToFirst()) {
                    it.apply {
                        firstName = c.getString(0)
                        middleName = c.getString(1)
                        lastName = c.getString(2)
                    }
                }
            }

            contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(
                    ContactsContract.CommonDataKinds.Organization.COMPANY,
                    ContactsContract.CommonDataKinds.Organization.TITLE
                ),
                ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                arrayOf(
                    it.id.toString(),
                    ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
                ),
                null
            ).use { c ->
                if (c != null && c.moveToFirst()) {
                    it.apply {
                        jobCompany = c.getString(0)
                        jobTitle = c.getString(1)
                    }
                }
            }

            contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(
                    ContactsContract.CommonDataKinds.Note.NOTE
                ),
                ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                arrayOf(
                    it.id.toString(),
                    ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE
                ),
                null
            ).use { c ->
                if (c != null && c.moveToFirst()) {
                    it.apply { note = c.getString(0) }
                }
            }
        }

        return result
    }

    companion object {
        const val EXTRA_PHONE = "EXTRA_PHONE"
    }
}