package com.victorlapin.dialer.ui.services

import android.content.Intent
import android.telecom.Call
import android.telecom.CallScreeningService
import android.telecom.TelecomManager

/**
 * Created by victor on 01.04.2022
 */
class ScreeningService : CallScreeningService() {
    override fun onScreenCall(callDetails: Call.Details) {
        if (callDetails.callDirection == Call.Details.DIRECTION_INCOMING) {
            respondToCall(callDetails, CallResponse.Builder().build())

            if (callDetails.handlePresentation == TelecomManager.PRESENTATION_ALLOWED) {
                val phoneNumber = callDetails.handle.schemeSpecificPart
                phoneNumber?.let {
                    val assistantIntent = Intent(this, AssistantService::class.java).apply {
                        putExtra(AssistantService.EXTRA_PHONE, it)
                    }
                    startService(assistantIntent)
                }
            }
        }
    }
}